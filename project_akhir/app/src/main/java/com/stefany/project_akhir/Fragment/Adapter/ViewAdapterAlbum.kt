package com.stefany.project_akhir.Fragment.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewAdapterAlbum (supportFragmentManager: FragmentManager):
    FragmentPagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
    private var ListFragment = ArrayList<Fragment>()
    private var ListIconFragment = ArrayList<String>()
    override fun getCount(): Int {
        return ListIconFragment.size
    }
    override fun getItem(position: Int): Fragment {
        return ListFragment[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ListIconFragment[position]
    }

    fun addFragment(fragment: Fragment, icon:String){
        ListFragment.add(fragment)
        ListIconFragment.add(icon)
    }
}