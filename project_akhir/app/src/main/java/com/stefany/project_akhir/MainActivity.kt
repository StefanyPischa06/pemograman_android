package com.stefany.project_akhir

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Fragment.Adapter.ViewPagerAdapter
import com.stefany.project_akhir.Fragment.AlbumFragment
import com.stefany.project_akhir.Fragment.TentangAndaFragment
import com.stefany.project_akhir.Fragment.UnggahanFragment
import com.stefany.project_akhir.Model.Album
import com.stefany.project_akhir.Utils.Util

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        setUpTabs()
        inputFirstImage()
    }

    private fun setUpTabs(){
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(TentangAndaFragment(),"Foto Tentang Anda")
        adapter.addFragment(UnggahanFragment(),"Unggahan")
        adapter.addFragment(AlbumFragment(),"Album")
        val viewPager = findViewById<ViewPager>(R.id.viewPager)
        val tabs = findViewById<TabLayout>(R.id.tabs)
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)
    }

    private fun inputFirstImage(){
        var album = ArrayList<Album>()
        album = DatabaseHelper(applicationContext).getAllIdAlbum()
        if(album.size == 0){
            val bitmap: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.add)
            DatabaseHelper(applicationContext).addAlbum("Buat Album")
            DatabaseHelper(applicationContext).addImageAlbum(1, Util.getBytes(bitmap),"")
        }
    }
}