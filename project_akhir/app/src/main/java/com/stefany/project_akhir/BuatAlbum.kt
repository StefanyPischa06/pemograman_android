package com.stefany.project_akhir

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toolbar
import androidx.fragment.app.FragmentManager
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Fragment.AlbumFragment

class BuatAlbum : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buat_album)
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbaralbum)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        val BtnBuat = findViewById<Button>(R.id.BtnBuat)
        val EtAlbum = findViewById<EditText>(R.id.EtNamaAlbum)
        BtnBuat.setOnClickListener {
            DatabaseHelper(applicationContext).addAlbum(EtAlbum.text.toString())
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}