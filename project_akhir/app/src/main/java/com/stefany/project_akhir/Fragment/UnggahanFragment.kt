package com.stefany.project_akhir.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Model.ImageAlbum
import com.stefany.project_akhir.R
import com.stefany.project_akhir.RecyclerView.ImageAdapter

class UnggahanFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_unggahan, container, false)
        var image : ArrayList<ImageAlbum> = DatabaseHelper(requireContext()).getAllDataAlbum()!!
        val rvUnggahan = view.findViewById<RecyclerView>(R.id.rvUnggahan)
        val gridLayout = GridLayoutManager(requireContext(), 3,GridLayoutManager.VERTICAL,false)
        gridLayout.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position%4 == 0 ) {
                    3
                } else 1
            }
        })
        rvUnggahan?.layoutManager = gridLayout
        rvUnggahan?.setHasFixedSize(true)
        var adapter = ImageAdapter(image)
        rvUnggahan.adapter= adapter
        return view
    }

}