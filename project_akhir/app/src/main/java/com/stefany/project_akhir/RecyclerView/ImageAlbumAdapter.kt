package com.stefany.project_akhir.RecyclerView

import android.content.Intent
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.stefany.project_akhir.BuatAlbum
import com.stefany.project_akhir.DataAlbum
import com.stefany.project_akhir.Model.Image
import com.stefany.project_akhir.Model.ImageAlbum
import com.stefany.project_akhir.R
import com.stefany.project_akhir.Utils.Util
import org.w3c.dom.Text

class ImageAlbumAdapter(val listImage:ArrayList<Image>): RecyclerView.Adapter<ImageAlbumAdapter.ImageAlbumHolder>() {
    class ImageAlbumHolder(val v : View):RecyclerView.ViewHolder(v){
        fun bindView(image : Image){
            v.findViewById<TextView>(R.id.tvAlbumNama).text = image.Judul
            if (image.Id_Album == 1){
                v.findViewById<ImageView>(R.id.ivImageAlbum).setImageBitmap(Util.getImage(image.Cover!!))
                v.findViewById<View>(R.id.view1).setBackgroundColor(Color.parseColor("#1454AE"))
                v.findViewById<View>(R.id.view2).setBackgroundColor(Color.parseColor("#1454AE"))
                v.findViewById<TextView>(R.id.tvAlbumNama).setTextColor(Color.parseColor("#1E90FF"))
                v.findViewById<ImageView>(R.id.ivImageAlbum).setBackgroundResource(R.drawable.border_image_blue)
                v.findViewById<ImageView>(R.id.ivImageAlbum).setOnClickListener {
                    val intent = Intent(v.context, BuatAlbum::class.java)
                    v.context.startActivity(intent)
                }
            }else if(image.Jumlah_Foto == 0){
                v.findViewById<ImageView>(R.id.ivImageAlbum).setImageResource(R.drawable.picture)
                v.findViewById<ImageView>(R.id.ivImageAlbum).setOnClickListener {
                    val intent = Intent(v.context, DataAlbum::class.java)
                    intent.putExtra("Id_Album", image.Id_Album)
                    intent.putExtra("judul",image.Judul)
                    v.context.startActivity(intent)
                }
                v.findViewById<TextView>(R.id.tvJumlahFoto).text = image.Jumlah_Foto.toString()
                v.findViewById<TextView>(R.id.tvFormat).text = "Foto"
            }else {
                v.findViewById<ImageView>(R.id.ivImageAlbum).setImageBitmap(Util.getImage(image.Cover!!))
                v.findViewById<ImageView>(R.id.ivImageAlbum).setOnClickListener {
                    val intent = Intent(v.context, DataAlbum::class.java)
                    intent.putExtra("Id_Album", image.Id_Album)
                    intent.putExtra("judul",image.Judul)
                    v.context.startActivity(intent)
                }
                v.findViewById<TextView>(R.id.tvJumlahFoto).text = image.Jumlah_Foto.toString()
                v.findViewById<TextView>(R.id.tvFormat).text = "Foto"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageAlbumHolder {
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.item_image_album,parent,false)
        return ImageAlbumHolder(v)
    }

    override fun onBindViewHolder(holder: ImageAlbumHolder, position: Int) {
        holder.bindView(listImage[position])
    }

    override fun getItemCount(): Int {
        return listImage.size
    }
}

