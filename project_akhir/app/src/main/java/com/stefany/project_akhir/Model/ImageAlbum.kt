package com.stefany.project_akhir.Model

import android.graphics.Bitmap

class ImageAlbum {
    var idFoto:Int?=null
    var idAlbum:Int?=null
    var foto:ByteArray?=null
    var keterangan:String?=null

    constructor(idFoto: Int?, idAlbum: Int?, foto: ByteArray?, keterangan: String?) {
        this.idFoto = idFoto
        this.idAlbum = idAlbum
        this.foto = foto
        this.keterangan = keterangan
    }

}