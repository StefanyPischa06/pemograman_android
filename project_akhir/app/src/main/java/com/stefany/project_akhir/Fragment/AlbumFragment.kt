package com.stefany.project_akhir.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Model.Image
import com.stefany.project_akhir.R
import com.stefany.project_akhir.RecyclerView.ImageAlbumAdapter

class AlbumFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val view = inflater.inflate(R.layout.fragment_album, container, false)
        var image : ArrayList<Image> = DatabaseHelper(requireContext()).getDataAlbumCover()
        val rvAlbum = view.findViewById<RecyclerView>(R.id.rvAlbum)
        rvAlbum.layoutManager = LinearLayoutManager(activity)
        rvAlbum?.layoutManager = GridLayoutManager(activity,2)
        rvAlbum?.setHasFixedSize(true)
        var adapter = ImageAlbumAdapter(image)
        rvAlbum.adapter= adapter
        return  view
    }

}