package com.stefany.project_akhir

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.stefany.project_akhir.Fragment.*
import com.stefany.project_akhir.Fragment.Adapter.ViewAdapterAlbum
import com.stefany.project_akhir.Fragment.Adapter.ViewPagerAdapter


class DataAlbum : AppCompatActivity() {
    var tabLayout: TabLayout? = null
    var frameLayout: FrameLayout? = null
    var fragment: Fragment? = null
    var fragmentManager: FragmentManager? = null
    var fragmentTransaction: FragmentTransaction? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_album)

        var id_album = intent.getIntExtra("Id_Album",0)
        var judul = intent.getStringExtra("judul")
        val judulAlbum = findViewById<TextView>(R.id.TvNamaAlbum)
        judulAlbum.setText(judul)
//        setUpTabs(id_album)

        val tabLayout = findViewById<TabLayout>(R.id.TlDataAlbum)
        val frameLayout = findViewById<FrameLayout>(R.id.frameLayout)

        fragment = ShowDataAlbum()
        var data = Bundle()
        data.putInt("data", id_album)
        fragment!!.arguments = data
        fragmentManager = supportFragmentManager
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.replace(R.id.frameLayout, fragment!!)
        fragmentTransaction!!.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction!!.commit()

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                // creating cases for fragment
                when (tab.position) {
                    0 -> fragment = ShowDataAlbum()
                    1 -> fragment = ListTambahanAlbum()
                }
                var data = Bundle()
                data.putInt("data", id_album)
                fragment!!.arguments = data
                val fm = supportFragmentManager
                val ft = fm.beginTransaction()
                ft.replace(R.id.frameLayout, fragment!!)
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

//    private fun setUpTabs(id_album:Int){
//        val adapter = ViewAdapterAlbum(supportFragmentManager)
//        val myFragment = ShowDataAlbum()
//        val data:Bundle = Bundle()
//        data.putInt("Id_Album", id_album)
//        myFragment.arguments = data
//        println("cobaaaaaaaaaaaa"+data.toString())
//        adapter.addFragment(ShowDataAlbum(),"")
//        adapter.addFragment(ListTambahanAlbum(),"")
//        val viewPager = findViewById<ViewPager>(R.id.ViewPagerDataAlbum)
//        val tabs = findViewById<TabLayout>(R.id.TlDataAlbum)
//        viewPager.adapter = adapter
//        tabs.setupWithViewPager(viewPager)
//
//        tabs.getTabAt(0)!!.setIcon(R.drawable.ic_grid)
//        tabs.getTabAt(1)!!.setIcon(R.drawable.ic_list)
//    }
}

