package com.stefany.project_akhir.Fragment.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter (supportFragmentManager: FragmentManager):FragmentPagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
    private var ListFragment = ArrayList<Fragment>()
    private var ListHalamanFragment = ArrayList<String>()
    private var ListIconFragment = ArrayList<Int>()
    override fun getCount(): Int {
        return ListHalamanFragment.size
    }
    override fun getItem(position: Int): Fragment{
        return ListFragment[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ListHalamanFragment[position]
    }

    fun addFragment(fragment: Fragment, halaman:String){
        ListFragment.add(fragment)
        ListHalamanFragment.add(halaman)
    }

    fun addFragment2(fragment: Fragment,icon:Int){
        ListFragment.add(fragment)
        ListIconFragment.add(icon)
    }
}