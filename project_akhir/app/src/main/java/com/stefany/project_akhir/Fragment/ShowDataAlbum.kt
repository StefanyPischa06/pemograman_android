package com.stefany.project_akhir.Fragment

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Model.Image
import com.stefany.project_akhir.Model.ImageAlbum
import com.stefany.project_akhir.R
import com.stefany.project_akhir.RecyclerView.ImageAdapter
import com.stefany.project_akhir.RecyclerView.ImageAlbumAdapter
import com.stefany.project_akhir.UploadImage
import com.stefany.project_akhir.Utils.Util
import java.io.ByteArrayOutputStream
import java.io.InputStream

class ShowDataAlbum:Fragment() {
    var idAlbum:Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_show_data_album, container, false)

        idAlbum= arguments?.getInt("data")
        if (idAlbum != null){
            var image : ArrayList<ImageAlbum>? = DatabaseHelper(requireContext()).getDataAlbum(idAlbum!!)
            val rvDataAlbum= view.findViewById<RecyclerView>(R.id.rvDataAlbum)
            rvDataAlbum.layoutManager = LinearLayoutManager(activity)
            rvDataAlbum?.layoutManager = GridLayoutManager(activity,3)
            rvDataAlbum?.setHasFixedSize(true)
            var adapter = ImageAdapter(image!!)
            rvDataAlbum.adapter= adapter
        }


        val TvTambah = view.findViewById<TextView>(R.id.TvTambah)
        TvTambah.setOnClickListener {
            imagePickDialog()
        }
        return view
    }

    private fun imagePickDialog(){
        val pilihan = arrayOf("Camera","Gallery")
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Ambil Dari")
        builder.setItems(pilihan){dialog, which ->
            if(which == 0){
                pickCamera()
            }else{
                pickGallery()
            }
        }
        builder.show()
    }

    private fun pickCamera(){
        var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 123)
    }

    private fun pickGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,456)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123){
            var image = data?.extras?.get("data") as Bitmap
            var intent = Intent(requireContext(), UploadImage::class.java)
            intent.putExtra("data",Util.getBytes(image))
            intent.putExtra("Id_Album",idAlbum)
            startActivity(intent)
        }else if(requestCode == 456){
            val image:Uri? = data?.data
            var os = ByteArrayOutputStream()
            var inputStream = context?.contentResolver?.openInputStream(image!!)
            var byteArray = inputStream?.readBytes()
            var intent = Intent(requireContext(), UploadImage::class.java)
            intent.putExtra("data",byteArray)
            intent.putExtra("Id_Album",idAlbum)
            startActivity(intent)
        }
    }
}