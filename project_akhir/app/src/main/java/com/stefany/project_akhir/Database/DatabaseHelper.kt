package com.stefany.project_akhir.Database

import android.content.ContentValues
import android.content.Context
import android.content.res.Resources
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.graphics.BitmapFactory
import androidx.fragment.app.Fragment
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import com.stefany.project_akhir.Fragment.UnggahanFragment
import com.stefany.project_akhir.Model.Album
import com.stefany.project_akhir.Model.Image
import com.stefany.project_akhir.Model.ImageAlbum
import com.stefany.project_akhir.R
import com.stefany.project_akhir.Utils.Util
import kotlin.jvm.Throws

class DatabaseHelper (context: Context): SQLiteAssetHelper(context, DB_NAME,null, DB_VER){
    companion object{
        private val DB_NAME = "ProjectAkhir.db"
        private  val DB_VER = 1

        private val TBL_NAME_ALBUM = "Album"
        private val COL_ID_ALBUM = "ID"
        private val COL_NAMA_ALBUM = "Nama"

        private val TBL_NAME_IMAGEALBUM = "ImageAlbum"
        private val COL_ID_IMAGE = "ID"
        private val COL_ID_ALBUMIMAGE = "ID_Album"
        private val COL_IMAGE = "Image"
        private val COL_KETERANGAN = "Keterangan"

    }

    @Throws(SQLiteException::class)

    fun addAlbum(name:String){
        val database : SQLiteDatabase = this.writableDatabase
        if(name.equals("")){
            val cv = ContentValues()
            cv.put(COL_NAMA_ALBUM, "Album Tanpa Judul")
            database.insert(TBL_NAME_ALBUM, null, cv)
        }else{
            val cv = ContentValues()
            cv.put(COL_NAMA_ALBUM, name)
            database.insert(TBL_NAME_ALBUM, null, cv)
        }
    }

    fun getAllIdAlbum():ArrayList<Album>{
        val db : SQLiteDatabase = this.writableDatabase
        val qb = SQLiteQueryBuilder()
        val sqlSelect : Array<String> = arrayOf(COL_ID_ALBUM, COL_NAMA_ALBUM)
        qb.tables = TBL_NAME_ALBUM
        val c = qb.query(db , sqlSelect, null, null, null,null,null)
        var size = ArrayList<Album>()
        if (c.moveToFirst()){
            do {
                size.add(Album(c.getInt(0), c.getString(1)))
            }while (c.moveToNext())
        }
        return size
    }

    fun getDataAlbumCover():ArrayList<Image>{
        var album:ArrayList<Album> = getAllIdAlbum()
        var image = ArrayList<Image>()
        var bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.picture)
        for (i in 0 until album.size){
            var data = getSizeDataAlbum(album[i].idAlbum!!)
            if (data == null){
                image.add(Image(album[i].idAlbum,album[i].namaAlbum,0))
            }else{
                image.add(Image(album[i].idAlbum,album[i].namaAlbum,data,showCoverAlbum(album[i].idAlbum!!)))
            }
        }
        return image
    }

    fun showCoverAlbum(idAlbum: Int): ByteArray?{
        val db : SQLiteDatabase = this.writableDatabase
        val qb = SQLiteQueryBuilder()
        val sqlSelect : Array<String> = arrayOf(COL_IMAGE)
        qb.tables = TBL_NAME_IMAGEALBUM
        val c = qb.query(db , sqlSelect, "ID_Album = ?", arrayOf(idAlbum.toString()), null,null,null)
        var result:ByteArray?=null
        var position = 0
        if (c.moveToFirst()){
            do {
                result = c.getBlob(c.getColumnIndex(COL_IMAGE))
            }while (position < 0)
        }
        return result
    }

    fun getDataAlbum(idAlbum:Int):ArrayList<ImageAlbum>?{
        val db : SQLiteDatabase = this.writableDatabase
        val qb = SQLiteQueryBuilder()
        val sqlSelect = arrayOf(COL_ID_IMAGE, COL_ID_ALBUMIMAGE, COL_IMAGE, COL_KETERANGAN)
        qb.tables = TBL_NAME_IMAGEALBUM
        val c = qb.query(db, sqlSelect, "ID_Album = ?", arrayOf(idAlbum.toString()), null,null,null)
        var result = ArrayList<ImageAlbum>()
        while (c.moveToNext()){
            result.add(ImageAlbum(c.getInt(0),c.getInt(1), c.getBlob(2),c.getString(3)))
        }
        return result
    }

    fun getAllDataAlbum():ArrayList<ImageAlbum>?{
        val db : SQLiteDatabase = this.writableDatabase
        val qb = SQLiteQueryBuilder()
        val sqlSelect = arrayOf(COL_ID_IMAGE, COL_ID_ALBUMIMAGE, COL_IMAGE, COL_KETERANGAN)
        qb.tables = TBL_NAME_IMAGEALBUM
        val c = qb.query(db, sqlSelect, null, null, null,null,null)
        var result = ArrayList<ImageAlbum>()
        while (c.moveToNext()){
            if(c.getInt(1)!=1){
                result.add(ImageAlbum(c.getInt(0),c.getInt(1), c.getBlob(2),c.getString(3)))
            }
        }
        return result
    }

    fun addImageAlbum(idAlbum: Int, image:ByteArray, keterangan:String){
        val database : SQLiteDatabase = this.writableDatabase
        if(keterangan.equals("")){
            val cv = ContentValues()
            cv.put(COL_ID_ALBUMIMAGE, idAlbum)
            cv.put(COL_IMAGE, image)
            cv.put(COL_KETERANGAN, " ")
            database.insert(TBL_NAME_IMAGEALBUM, null, cv)
        }else{
            val cv = ContentValues()
            cv.put(COL_ID_ALBUMIMAGE, idAlbum)
            cv.put(COL_IMAGE, image)
            cv.put(COL_KETERANGAN, keterangan)
            database.insert(TBL_NAME_IMAGEALBUM, null, cv)
        }
    }

    fun getSizeDataAlbum(idAlbum: Int):Int{
        val db : SQLiteDatabase = this.writableDatabase
        val qb = SQLiteQueryBuilder()
        val sqlSelect : Array<String> = arrayOf(COL_ID_ALBUM)
        qb.tables = TBL_NAME_IMAGEALBUM
        val c = qb.query(db , sqlSelect, "ID_Album = ?", arrayOf(idAlbum.toString()), null,null,null)
        var size = 0
        if (c.moveToFirst()){
            do {
                size++
            }while (c.moveToNext())
        }
        return size
    }

    fun deleteImageAlbum(idFoto:Int): Int{
        val db : SQLiteDatabase = this.writableDatabase
        return db.delete(TBL_NAME_IMAGEALBUM,"ID = ?", arrayOf(idFoto.toString()))

    }

    fun editKeterangan(idAlbum: Int, idFoto:Int,image:ByteArray, keterangan:String):Boolean{
        val database : SQLiteDatabase = this.writableDatabase
        if(keterangan.equals("")){
            val cv = ContentValues()
            cv.put(COL_ID_ALBUMIMAGE, idAlbum)
            cv.put(COL_IMAGE, image)
            cv.put(COL_KETERANGAN, " ")
            database.update(TBL_NAME_IMAGEALBUM,cv,"ID = ?", arrayOf(idFoto.toString()))
        }else{
            val cv = ContentValues()
            cv.put(COL_ID_ALBUMIMAGE, idAlbum)
            cv.put(COL_IMAGE, image)
            cv.put(COL_KETERANGAN, keterangan)
            database.update(TBL_NAME_IMAGEALBUM,cv,"ID = ?", arrayOf(idFoto.toString()))
        }
        return true
    }


}