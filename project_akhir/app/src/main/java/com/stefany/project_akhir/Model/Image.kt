package com.stefany.project_akhir.Model

class Image {
    var Id_Album:Int?=null
    var Judul:String? = null
    var Jumlah_Foto:Int?=null
    var Cover: ByteArray?=null
    constructor(){

    }

    constructor(Id_Album: Int?, Judul: String?, Jumlah_Foto: Int?, Cover: ByteArray?) {
        this.Id_Album = Id_Album
        this.Judul = Judul
        this.Jumlah_Foto = Jumlah_Foto
        this.Cover = Cover
    }

    constructor(Id_Album: Int?, Judul: String?, Jumlah_Foto: Int?) {
        this.Id_Album = Id_Album
        this.Judul = Judul
        this.Jumlah_Foto = Jumlah_Foto
    }


}