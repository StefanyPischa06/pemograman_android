package com.stefany.project_akhir

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Utils.Util

class ImageFullActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_full)

        var id_album = intent.getIntExtra("Id_Album",0)
        var id_foto = intent.getIntExtra("Id_Foto", 0)
        var foto = intent.getByteArrayExtra("foto")
        var keterangan = intent.getStringExtra("keterangan")

        val image = findViewById<ImageView>(R.id.foto).setImageBitmap(Util.getImage(foto!!))
        val ket = findViewById<EditText>(R.id.keterangan)
        ket.setText(keterangan)
        val delete = findViewById<ImageView>(R.id.delete)
        val edit = findViewById<Button>(R.id.edit)

        delete.setOnClickListener {
            var t = DatabaseHelper(applicationContext).deleteImageAlbum(id_foto)
            if(t == 1){
                Toast.makeText(this,"Foto Berhasil dihapus",Toast.LENGTH_SHORT ).show()
            }else{
                Toast.makeText(this,"Foto Gagal dihapus",Toast.LENGTH_SHORT ).show()
            }
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        edit.setOnClickListener {
            if(ket.text.toString().equals(keterangan)){
                Toast.makeText(this, "Keterangan tidak berubah", Toast.LENGTH_SHORT).show()
            }else{
                var t = DatabaseHelper(applicationContext).editKeterangan(id_album,id_foto,foto,ket.text.toString())
                if(t){
                    Toast.makeText(this, "Keterangan berhasil diubah", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }else{
                    Toast.makeText(this,"Keterangan gagal diubah", Toast.LENGTH_SHORT).show()
                }
            }

        }

    }
}