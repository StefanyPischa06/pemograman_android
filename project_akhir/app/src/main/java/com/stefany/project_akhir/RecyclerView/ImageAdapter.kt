package com.stefany.project_akhir.RecyclerView

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.stefany.project_akhir.ImageFullActivity
import com.stefany.project_akhir.MainActivity
import com.stefany.project_akhir.Model.ImageAlbum
import com.stefany.project_akhir.R
import com.stefany.project_akhir.Utils.Util

class ImageAdapter(val listImageAlbum:ArrayList<ImageAlbum>):RecyclerView.Adapter<ImageAdapter.ImageHolder>(){
    class ImageHolder(val v:View):RecyclerView.ViewHolder(v){
        fun bindView(image : ImageAlbum){
            v.findViewById<ImageView>(R.id.ivImage).setImageBitmap(Util.getImage(image.foto!!))
            v.findViewById<ImageView>(R.id.ivImage).setOnClickListener {
                val intent = Intent(v.context, ImageFullActivity::class.java)
                intent.putExtra("Id_Album", image.idAlbum)
                intent.putExtra("Id_Foto", image.idFoto)
                intent.putExtra("foto",image.foto)
                intent.putExtra("keterangan", image.keterangan)
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.item_image,parent,false)
        return ImageHolder(v)
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.bindView(listImageAlbum[position])
    }

    override fun getItemCount(): Int {
        return listImageAlbum.size
    }
}