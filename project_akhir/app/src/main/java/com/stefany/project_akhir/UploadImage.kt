package com.stefany.project_akhir

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.stefany.project_akhir.Database.DatabaseHelper
import com.stefany.project_akhir.Utils.Util

class UploadImage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_image)
        var id = intent.getIntExtra("Id_Album",0)
        var data = intent.getByteArrayExtra("data")

        val image = findViewById<ImageView>(R.id.imageUpload)
        image.setImageBitmap(Util.getImage(data!!))
        var keterangan = findViewById<EditText>(R.id.EtKeterangan)
        var upload = findViewById<Button>(R.id.btnPosting)
        upload.setOnClickListener {
            DatabaseHelper(applicationContext).addImageAlbum(id,data,keterangan.text.toString())
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}