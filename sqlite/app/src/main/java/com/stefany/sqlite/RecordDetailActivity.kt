package com.stefany.sqlite

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import kotlinx.android.synthetic.main.activity_add_update_record.*
import kotlinx.android.synthetic.main.activity_record_detail.*
import java.util.*

class RecordDetailActivity : AppCompatActivity() {
    //actionBar
    private var actionBar:ActionBar?=null
    //db Helper
    private var dbHelper:MyDbHelper?=null
    private  var recordId:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_detail)

        //setting up action bar
        actionBar = supportActionBar
        actionBar!!.title ="Record Details"
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar!!.setDisplayShowHomeEnabled(true)

        //init db Helper
        dbHelper = MyDbHelper(this)

        //get record id from intent
        val intent = intent
        recordId = intent.getStringExtra("RECORD_ID")
        showRecordDetails()
    }

    private fun showRecordDetails() {
        //get record
        val selectQuery = "SELECT * FROM ${Constants.TABLE_NAME} WHERE ${Constants.C_ID} =\"$recordId\""
        val db = dbHelper!!.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        if(cursor.moveToFirst()){
            do{
                val id =""+cursor.getString(cursor.getColumnIndex(Constants.C_ID))
                val name = ""+cursor.getString(cursor.getColumnIndex(Constants.C_NAME))
                val image = ""+cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE))
                val bio = ""+cursor.getString(cursor.getColumnIndex(Constants.C_BIO))
                val phone=""+cursor.getString(cursor.getColumnIndex(Constants.C_PHONE))
                val email=""+cursor.getString(cursor.getColumnIndex(Constants.C_EMAIL))
                val dob=""+cursor.getString(cursor.getColumnIndex(Constants.C_DOB))
                val addedTimestamp =""+cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP))
                val updatedTimestamp =""+cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP))

                //convert timestamp
                val calender1 = Calendar.getInstance(Locale.getDefault())
                calender1.timeInMillis = addedTimestamp.toLong()
                val timeAdded = android.text.format.DateFormat.format("dd/MM/yyyy hh:mm:aa",calender1)

                val calender2 = Calendar.getInstance(Locale.getDefault())
                calender2.timeInMillis = updatedTimestamp.toLong()
                val timeUpdated = android.text.format.DateFormat.format("dd/MM/yyyy hh:mm:aa",calender2)

                //set data
                nameTv.text = name
                bioTv.text = bio
                phoneTv.text = phone
                emailTv.text = email
                dobTv.text = dob
                addedDateTv.text = timeAdded
                updatedDateTv.text = timeUpdated

                if(image == "null"){
                    fotoIv.setImageResource(R.drawable.ic_person)
                }else{
                    fotoIv.setImageURI(Uri.parse(image))
                }

            }while(cursor.moveToNext())
        }
        //close db collection
        db.close()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}