package com.stefany.sqlite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import kotlinx.android.synthetic.main.activity_main.*
import android.app.SearchManager
import android.widget.SearchView.OnQueryTextListener

class MainActivity : AppCompatActivity() {
    //db Helper
    lateinit var dbHelper:MyDbHelper
    //order by
    private val NEWEST_FIRST = Constants.C_ADDED_TIMESTAMP + " DESC"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //init db
        dbHelper = MyDbHelper(this)

        loadRecords()

        addRecordBtn.setOnClickListener {
            startActivity(Intent(this,AddUpdateRecordActivity::class.java))
        }
    }

    private fun loadRecords() {
        val adapterRecord = AdapterRecord(this, dbHelper.getAllRecords(NEWEST_FIRST))
        recordRv.adapter = adapterRecord
    }

    private fun searchRecords(query:String) {
        val adapterRecord = AdapterRecord(this, dbHelper.searchRecords(query))
        recordRv.adapter = adapterRecord
    }

    override fun onResume() {
        super.onResume()
        loadRecords()
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.menu_main, menu)
//        val item = menu.findItem(R.id.action_search)
//        val searchView = item.actionView as SearchView
//        searchView.setOnQueryTextListener(object :
//          SearchView.OnQueryTextListener{
//            override fun onQueryTextChange(newText: String?): Boolean {
//                // search sesuai tipe mu
//                if(newText != null){
//                    searchRecords(newText)
//                }
//                return true
//            }
//
//            override fun onQueryTextSubmit(query: String?): Boolean {
//               //serch when button search click
//                if(query != null){
//                    searchRecords(query)
//                }
//                return true
//            }
//          }
//        )
//        return super.onCreateOptionsMenu(menu)
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }
}