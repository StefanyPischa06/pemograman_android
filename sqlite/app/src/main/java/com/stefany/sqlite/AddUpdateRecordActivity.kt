package com.stefany.sqlite

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_add_update_record.*
import java.util.jar.Manifest

class AddUpdateRecordActivity : AppCompatActivity() {
    //permission constants
    private val CAMERA_REQUEST_CODE = 100
    private val STORAGE_REQUEST_CODE =101
    //image pick constants
    private val IMAGE_PICK_CAMERA_CODE =102
    private val IMAGE_PICK_GALLERY_CODE = 103
    //array of permissions
    private lateinit var cameraPermissions:Array<String>
    private lateinit var storagePermission:Array<String>
    //action bar
    private var actionBar:ActionBar?=null
    lateinit var dbHelper :MyDbHelper
    //variable that will contain data to save in database
    private var imageUri:Uri?  = null
    private var name:String?=""
    private var phone:String?=""
    private var email:String?=""
    private var dob:String?=""
    private var bio:String?=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_update_record)
        //init actionbar
        actionBar = supportActionBar

        actionBar!!.title="Add Record"
        //title of actionbar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.setDisplayShowHomeEnabled(true)
        //init db
        dbHelper = MyDbHelper(this)
        //init permission arrays
        cameraPermissions = arrayOf(android.Manifest.permission.CAMERA,
        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        storagePermission = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

        //click imageviewto pick image
        profileIv.setOnClickListener {
            imagePickDialog()
        }
        //click savebtn to save record
        saveBtn.setOnClickListener {
            inputData()
        }
    }

    private fun inputData() {
        //get data
        name =""+nameEt.text.toString().trim()
        phone=""+phoneEt.text.toString().trim()
        email=""+emailEt.text.toString().trim()
        dob=""+dobEt.text.toString().trim()
        bio=""+bioEt.text.toString().trim()

        //input data to db
        val timestamp = System.currentTimeMillis()
        val id = dbHelper.insertRecord(
            ""+name,
            ""+imageUri,
            ""+bio,
            ""+phone
        ,   ""+email,
            ""+dob,
            ""+timestamp,
            ""+timestamp)
        Toast.makeText(this,"Record Added against ID $id", Toast.LENGTH_SHORT).show()
    }

    private fun imagePickDialog() {
        //options to display in dialog
        val options =  arrayOf("Camera","Gallery")
        //dialog
        val builder =  AlertDialog.Builder(this)
        //title
        builder.setTitle("Pick Image")
        //set items/option
        builder.setItems(options){ dialog, which ->
            if(which==0){
                //camera click
                if(!checkCameraPermission()){
                    requestCameraPermission()
                }else{
                    pickFromCamera()
                }
            }else{
                if (!checkStoragePermission()){
                    requestStoragePermission()
                }else{
                    pickFromStorage()
                }
            }
        }
        //show dialog
        builder.show()
    }

    private fun pickFromStorage() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type ="image/*"
        startActivityForResult(
            galleryIntent,
            IMAGE_PICK_GALLERY_CODE
        )
    }

    private fun requestStoragePermission() {
        ActivityCompat.requestPermissions(this,storagePermission,STORAGE_REQUEST_CODE)
    }

    private fun checkStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        )== PackageManager.PERMISSION_GRANTED
    }

    private fun pickFromCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE,"Image Title")
        values.put(MediaStore.Images.Media.DESCRIPTION,"Image Description")
        imageUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(
            cameraIntent,
            IMAGE_PICK_CAMERA_CODE
        )
    }

    private fun requestCameraPermission() {
        ActivityCompat.requestPermissions(this,cameraPermissions,CAMERA_REQUEST_CODE)
    }

    private fun checkCameraPermission(): Boolean {
        val results = ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.CAMERA
        )== PackageManager.PERMISSION_GRANTED
        val resultsq = ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.CAMERA
        )== PackageManager.PERMISSION_GRANTED
        return  results && resultsq
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()//go back previsious activity
        return super.onSupportNavigateUp()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            CAMERA_REQUEST_CODE->{
                if(grantResults.isNotEmpty()){
                    val cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    val storageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    if (cameraAccepted && storageAccepted){
                        pickFromCamera()
                    }else{
                        Toast.makeText(this,"Camera and Storage permissions are requires", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            STORAGE_REQUEST_CODE->{
                if(grantResults.isNotEmpty()){
                    val storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    if (storageAccepted){
                        pickFromStorage()
                    }else{
                        Toast.makeText(this,"Storage permissions is requires", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //image pick from gallery or camera will be received here
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == IMAGE_PICK_GALLERY_CODE){
                CropImage.activity(data!!.data)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this)
            }
            else if(requestCode == IMAGE_PICK_CAMERA_CODE){
                CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this)
            }
            else if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK){
                    val resultUri = result.uri
                    imageUri = resultUri
                    profileIv.setImageURI(resultUri)
                }
                else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                    val error = result.error
                    Toast.makeText(this,""+error, Toast.LENGTH_SHORT).show()
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}