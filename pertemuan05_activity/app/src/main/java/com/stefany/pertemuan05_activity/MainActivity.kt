package com.stefany.pertemuan05_activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val username = intent.getStringExtra("username")
        val tvGeterring = findViewById<TextView>(R.id.tvGreetings)
        tvGeterring.text = "Selamat datang $username"
    }
}