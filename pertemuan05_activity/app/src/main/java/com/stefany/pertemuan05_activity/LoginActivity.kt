package com.stefany.pertemuan05_activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class LoginActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //mengcloning id di tag pada layout
        val etUsername = findViewById<EditText>(R.id.etUsername)
        val etPassword = findViewById<EditText>(R.id.etPassword)
        val btnLogin = findViewById<Button>(R.id.btnLogin)

//        val username = etUsername.text
//        val password = etPassword.text

        //segala sesuatu yang berhubungan dengan event menggunakan listener
        btnLogin.setOnClickListener {
            if (etPassword.text.toString().equals("1234")){
                toast_pesan("Login berhasil")
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("username", etUsername.text.toString())
                startActivity(intent)
            }else{
                toast_pesan("Login Gagal")
            }
        }
    }

    fun toast_pesan(message : String){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}