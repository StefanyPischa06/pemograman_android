package com.stefany.pertemuan12_apiservices

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    lateinit var queue:RequestQueue
    //hanya base url
    val url = "https://api.openweathermap.org/data/2.5/"
    val apiKey = "2120b669b6a05f06e89cb02de940d3d0"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        queue = Volley.newRequestQueue(this)

        val etKota = findViewById<EditText>(R.id.etKota)
        val btnCekCuasa = findViewById<Button>(R.id.btnCekCuaca)

        btnCekCuasa.setOnClickListener {
            cekCuaca(etKota.text.toString())
        }
    }

    fun cekCuaca(kota : String){
        val tvHasil = findViewById<TextView>(R.id.tvHasil)
        val pd = ProgressDialog(this)
        pd.show()
        //kalau mengirim data ke dalam body pakau method post, kalau pakek get nanti datanya dikirim ke url
        val request = StringRequest(Request.Method.GET,
                "${url}weather?q=${kota}&appid=${apiKey}",
                Response.Listener <String>{response->
                    pd.dismiss()
                    val json = JSONObject(response)
                    //getJSONArray digunakan untuk mengambil data di dalam kurung siku []
                    //getJSONObject digunakan untuk mengambil data di dalam kurung kurawal {}
                    val main = json.getJSONArray("weather").getJSONObject(0).getString("main")
                    val temp = json.getJSONObject("main").getDouble("temp")-273.15
                    tvHasil.setText("Cuaca: ${main}\nSuhu: ${temp}\u2103")
                }, Response.ErrorListener {e->
                    Log.d("Error Volley", e.toString())
                    pd.dismiss()
                }
                )
        //proses pengiriman request ke server
        queue.add(request)
    }
}