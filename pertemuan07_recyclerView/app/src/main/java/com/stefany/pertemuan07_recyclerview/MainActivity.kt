package com.stefany.pertemuan07_recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listSong =ArrayList<Song>()
        listSong.add(Song(1,"Gone","Rose",R.mipmap.rose))
        listSong.add(Song(2,"Eight","IU & Suga",R.mipmap.iu))
        listSong.add(Song(3,"Imagination","Shawn Mendes",R.mipmap.sm))

        val rvSong = findViewById<RecyclerView>(R.id.rvSong)
        rvSong.layoutManager = LinearLayoutManager(this)
        rvSong?.layoutManager= GridLayoutManager(this,2)
        rvSong?.setHasFixedSize(true)
        val adapter = SongAdapter(this,listSong)
        rvSong.adapter = adapter

        listSong.add(Song(4,"Imagination","coba4",R.mipmap.sm))
        listSong.add(Song(5,"Eight","coba5",R.mipmap.iu))
        adapter.notifyDataSetChanged() //tugasnya mengupdate isi dari recycle view

//        rvSong.addOnScrollListener() // supaya waktu data sudah memenuhi layar ketika di scroll dapat di load ulang
    }
}