package com.stefany.pertemuan07_recyclerview

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class SongAdapter(val context: Context,val listSong:ArrayList<Song>):RecyclerView.Adapter<SongAdapter.SongHolder>(){
    class SongHolder(val v : View):RecyclerView.ViewHolder(v){
        fun bindView(song : Song){
            v.findViewById<TextView>(R.id.tvJudul).text = "#${song.rank} ${song.title}"
            v.findViewById<TextView>(R.id.tvPenyanyi).text ="${song.singer}"
            v.findViewById<ImageView>(R.id.ivCover).setImageResource(song.cover)
            if(song.rank == 1){
                v.findViewById<ImageView>(R.id.ivCover).setOnClickListener {
                    val i:Intent = Intent(v.context, details::class.java)
                    v.context.startActivity(i)
//                    Toast.makeText(v.context, "${song.title} play", Toast.LENGTH_LONG).show()
                }
            }else{
                v.findViewById<ImageView>(R.id.ivCover).setOnClickListener {
//                    println("coba")
                    Toast.makeText(v.context, "${song.title} play", Toast.LENGTH_LONG).show()
//                    val i:Intent = Intent(v.context, MainActivity::class.java)
//                    v.context.startActivity(i)
                }
            }
//            v.setOnClickListener {
//
//            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongAdapter.SongHolder {
        //menconvert xml ke dalam bentuk object supaya bisa diakses di kotlin
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.item_song,parent,false)
        return SongHolder(v)
    }

    override fun onBindViewHolder(holder: SongAdapter.SongHolder, position: Int) {
        //memasang data ke item
        if(position == 0){
            val intent = Intent(context, details::class.java)
        }
        holder.bindView(listSong[position])
    }

    override fun getItemCount(): Int {
       //menghitung data yang ditampilkan
        return listSong.size
    }

}

