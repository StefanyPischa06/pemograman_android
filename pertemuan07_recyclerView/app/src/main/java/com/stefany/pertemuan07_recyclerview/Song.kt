package com.stefany.pertemuan07_recyclerview

data class Song(val rank:Int, val title:String, val singer:String, val cover: Int)