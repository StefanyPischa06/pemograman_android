package com.stefany.pertemuan09_sharedpreference

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {
    var sp: SharedPreferences? = null
    var spEdit: SharedPreferences.Editor? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //inisialisasi sp
        sp = getSharedPreferences("mySP", Context.MODE_WORLD_READABLE)
        spEdit = sp?.edit()
        if(sp?.getBoolean("isLogin", false) == true){
            //jika sudah login
            setContentView(R.layout.activity_home)
            val spBahasa = findViewById<Spinner>(R.id.spBahasa)
            val adapter = ArrayAdapter.createFromResource(this,R.array.list_bahasa, R.layout.support_simple_spinner_dropdown_item)
            spBahasa.adapter = adapter
            spBahasa.setSelection(sp!!.getInt("bahasa",1))
            spBahasa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(p0: AdapterView<*>?, view: View, position: Int, id: Long) {
                    spEdit?.putInt("bahasa",position)
                    spEdit?.commit()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

            }
            val etUkuranFont = findViewById<EditText>(R.id.etUkuranFont)
            etUkuranFont.setText(sp!!.getString("ukuran","10"))
            etUkuranFont.addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    TODO("Not yet implemented")
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    TODO("Not yet implemented")
                }

                override fun afterTextChanged(p0: Editable?) {
                    spEdit?.putString("ukuran",p0.toString())
                    spEdit?.commit()
                }

            })
        }else{
            setContentView(R.layout.activity_main)
            val etUsername = findViewById<EditText>(R.id.etUsername)
            val etPassword = findViewById<EditText>(R.id.etPassword)
            val btLogin = findViewById<Button>(R.id.btLogin)
            btLogin.setOnClickListener{
                if(etUsername.text.toString().equals("admin")&&etPassword.text.toString().equals("1234")){
                    spEdit?.putBoolean("isLogin",true)
                    spEdit?.commit() // kembaliannya boleean kalau apply kembalian unit atau object
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}


class coba{
    val nama = " aku"
    val usia = 10

}