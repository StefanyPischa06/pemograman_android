package com.stefany.pertemuan13_databinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.stefany.pertemuan13_databinding.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        val bind:ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        bind.register = Register("","","","","","")
        val btnRegister = findViewById<Button>(R.id.btnregister)
        btnRegister.setOnClickListener {
            Toast.makeText(
                this,
                "${bind.register!!.namaDepan} ${bind.register!!.namaBelakang} (${bind.register!!.username})",
                Toast.LENGTH_LONG
            ).show()
            bind.register = Register("","","","","","")
        }
    }
}