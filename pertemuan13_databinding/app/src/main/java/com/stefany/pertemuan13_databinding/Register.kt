package com.stefany.pertemuan13_databinding

data class Register(
    var namaDepan:String,
    var namaBelakang:String,
    var username:String,
    var password:String,
    var ulangipassword:String,
    var email:String
)