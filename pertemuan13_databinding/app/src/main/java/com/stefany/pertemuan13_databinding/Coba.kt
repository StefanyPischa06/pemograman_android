package com.stefany.pertemuan13_databinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.stefany.pertemuan13_databinding.databinding.ActivityMainBinding

class Coba : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_coba)
        val bind: CobaBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }
}