package com.stefany.pertemuan11_firebase

import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firestore = FirebaseFirestore.getInstance()
        val p = Penduduk("Kristian Adi Nugraha", 17)
        firestore.collection("penduduk").add(p)
                .addOnSuccessListener{
            Log.d("TAG","penyimpanan selesai")
            }.addOnFailureListener{
            Log.d("TAG", "proses penyimpanan gagal")
            }


    }
}