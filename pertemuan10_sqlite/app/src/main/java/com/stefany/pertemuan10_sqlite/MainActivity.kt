package com.stefany.pertemuan10_sqlite

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    //membuat variable database sqlite
    //tidak bisa langsung instalasi
//    lateinit var db : SQLiteDatabase
    var t = DatabaseConnect()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //instansiasi
        t.db = DatabaseHelper(this).writableDatabase
        val etNama = findViewById<EditText>(R.id.etNama)
        val etUsia = findViewById<EditText>(R.id.etUsia)
        val btnSimpan = findViewById<Button>(R.id.btnSimpan)
        val btnHapus = findViewById<Button>(R.id.btnHapus)

        btnSimpan.setOnClickListener {
            t.insertData(Penduduk(etNama.text.toString(), etUsia.text.toString().toInt()))
            etNama.setText("")
            etUsia.setText("")
            show()
        }

        btnHapus.setOnClickListener {
            t.deleteData(Penduduk(etNama.text.toString(), etUsia.text.toString().toInt()))
            etNama.setText("")
            etUsia.setText("")
            show()
        }
        show()

    }
    fun show(){
        val tvHasil = findViewById<TextView>(R.id.tvHasil)
        tvHasil.text = t.getData()
    }
//    fun insertData(penduduk:Penduduk){
//        //apply termasuk fitur tambahan di kotlin digunakan untuk memsukan beberapa parameter
//        val values = ContentValues().apply {
//            put(DatabaseContract.Penduduk.COLUMN_NAME_NAMA,penduduk.nama)
//            put(DatabaseContract.Penduduk.COLUMN_NAME_USIA,penduduk.usia)
//        }
//        db.insert(DatabaseContract.Penduduk.TABLE_NAME,null, values)
//        getData()
//    }
//    fun deleteData(penduduk:Penduduk){
//        val selection = "${DatabaseContract.Penduduk.COLUMN_NAME_NAMA} LIKE ? OR ${DatabaseContract.Penduduk.COLUMN_NAME_USIA} = ?"
//        val selectionArg = arrayOf(penduduk.nama, penduduk.usia.toString())
//        db.delete(DatabaseContract.Penduduk.TABLE_NAME,selection,selectionArg)
//        getData()
//    }
//    fun getData(){
//        //membuat array yang isinya column2 yang mau diambil
//        val columns = arrayOf(
//            DatabaseContract.Penduduk.COLUMN_NAME_NAMA,
//            DatabaseContract.Penduduk.COLUMN_NAME_USIA
//        )
//        //membuat kursor seperti pointer pada linked list, kursor kembalian dari db
//        val cursor = db.query(
//            DatabaseContract.Penduduk.TABLE_NAME,
//            columns,
//            null,
//            null,
//            null,
//            null,
//            null
//        )
//
//        // untuk query manual pakai rawQuery()
//        var result =""
//        with(cursor){
//            while (moveToNext()){
//                val penduduk = Penduduk(getString(0),getInt(1))
//                result +="${penduduk.nama} - ${penduduk.usia}\n"
//            }
//        }
//        val tvHasil = findViewById<TextView>(R.id.tvHasil)
//        tvHasil.text = result
//    }
}