package com.stefany.pertemuan10_sqlite

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.widget.TextView

class DatabaseConnect {
    lateinit var db : SQLiteDatabase
    fun insertData(penduduk:Penduduk){
        //apply termasuk fitur tambahan di kotlin digunakan untuk memsukan beberapa parameter
        val values = ContentValues().apply {
            put(DatabaseContract.Penduduk.COLUMN_NAME_NAMA,penduduk.nama)
            put(DatabaseContract.Penduduk.COLUMN_NAME_USIA,penduduk.usia)
        }
        db.insert(DatabaseContract.Penduduk.TABLE_NAME,null, values)
        getData()
    }
    fun deleteData(penduduk:Penduduk){
        val selection = "${DatabaseContract.Penduduk.COLUMN_NAME_NAMA} LIKE ? OR ${DatabaseContract.Penduduk.COLUMN_NAME_USIA} = ?"
        val selectionArg = arrayOf(penduduk.nama, penduduk.usia.toString())
        db.delete(DatabaseContract.Penduduk.TABLE_NAME,selection,selectionArg)
        getData()
    }

    fun getData(): String {
        //membuat array yang isinya column2 yang mau diambil
        val columns = arrayOf(
                DatabaseContract.Penduduk.COLUMN_NAME_NAMA,
                DatabaseContract.Penduduk.COLUMN_NAME_USIA
        )
        //membuat kursor seperti pointer pada linked list, kursor kembalian dari db
        val cursor = db.query(
                DatabaseContract.Penduduk.TABLE_NAME,
                columns,
                null,
                null,
                null,
                null,
                null
        )

        // untuk query manual pakai rawQuery()
        var result =""
        with(cursor){
            while (moveToNext()){
                val penduduk = Penduduk(getString(0),getInt(1))
                result +="${penduduk.nama} - ${penduduk.usia}\n"
            }
        }
        return result
    }
}