package com.stefany.pertemuan10_sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(val context: Context):SQLiteOpenHelper(context, DATABASE_NAME, null,DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        //hanya dilakukan satu kali saat aplikasi diinstal
        db?.execSQL(DatabaseContract.Penduduk.SQL_CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, olVersion: Int, newVersion: Int) {
       //digunakan untuk ketika ada perlubahan versi
        db?.execSQL(DatabaseContract.Penduduk.SQL_DELETE_TABLE)
        //pindah data
        onCreate(db)
    }
    //companion object digunakan ketika suatu class bisa dibuat instansinya dan bisa diakses secara statistik
    companion  object{
        const val DATABASE_NAME = "myDB.abc"
        const val DATABASE_VERSION = 1
    }
    //class database helper digunakan untuk menghubungkan db di so dengan activity

}