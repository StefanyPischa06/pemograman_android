package com.stefany.pertemuan10_sqlite

import android.provider.BaseColumns

class DatabaseContract {
    //untuk setiap tabel harus ada objectny
    object Penduduk:BaseColumns{
        const val TABLE_NAME = "penduduk"
        const val COLUMN_NAME_NAMA = "nama"
        const val COLUMN_NAME_USIA = "usia"
        // kalau pakai const harus ada val, kecuali val aja bisa
        // untuk nilai yang langsung diisi pakailah const
        //untuk nilai yang diisi dari suatu fungsi pakailah val, jk pakai const tidak bisa diisi melalu fungsi
        const val SQL_CREATE_TABLE = "CREATE TABLE ${TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${COLUMN_NAME_NAMA} TEXT," +
                "${COLUMN_NAME_USIA} TEXT)"

        //query delete digunakan jika ada perubahan di dalam database misal versi atau nama
        const val SQL_DELETE_TABLE = "DROP TABLE IF EXISTS \${TABLE_NAME}"



    }
}