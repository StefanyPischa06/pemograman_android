package com.stefany.fragmenttablayout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [JavaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class JavaFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_java, container, false)
        var data:String? = arguments?.getString("data")
        view.findViewById<Button>(R.id.javaButton).setOnClickListener(View.OnClickListener {
            Toast.makeText(context,data, Toast.LENGTH_SHORT).show()
        })
        return view
    }


}