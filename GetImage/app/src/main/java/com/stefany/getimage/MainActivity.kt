package com.stefany.getimage

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.core.graphics.drawable.toDrawable
import com.stefany.getimage.DbHelper.DBHelper
import com.stefany.getimage.Utils.Util

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn_select = findViewById<Button>(R.id.btn_select)
        val btn_show = findViewById<Button>(R.id.btn_show)
        val bitmap: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.plus3)
        DBHelper(applicationContext).addBitmap("plus", Util.getBytes(bitmap))

        btn_select.setOnClickListener {
            val intent = Intent(this, SelectActivity::class.java)
            startActivity(intent)
        }

        btn_show.setOnClickListener {
            startActivity(Intent(this@MainActivity, ShowActivity::class.java))
        }
    }
}