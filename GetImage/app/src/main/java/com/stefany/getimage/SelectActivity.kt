package com.stefany.getimage

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.stefany.getimage.DbHelper.DBHelper
import com.stefany.getimage.Utils.Util

class SelectActivity : AppCompatActivity() {
    val SELECT_PHOTO = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select)
        val btn_select = findViewById<Button>(R.id.btn_select)
        val btn_save = findViewById<Button>(R.id.btn_save)
        val img_select = findViewById<ImageView>(R.id.img_select)

        btn_select.setOnClickListener {
            val photoPicker = Intent(Intent.ACTION_PICK)
            photoPicker.type = "image/*"
            startActivityForResult(photoPicker,SELECT_PHOTO)
        }

        btn_save.setOnClickListener {
            val btn_select = findViewById<Button>(R.id.btn_select)
            val btn_save = findViewById<Button>(R.id.btn_save)
            val img_select = findViewById<ImageView>(R.id.img_select)
            //create bitmap from image view
            val bitmap:Bitmap = (img_select.drawable as BitmapDrawable).bitmap
            val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this@SelectActivity)
            alertDialog.setTitle("Enter name of picture")
            val editText = EditText(this@SelectActivity)
            alertDialog.setView(editText)
            alertDialog.setPositiveButton("OK"){
                dialog, which ->
                if(!TextUtils.isEmpty(editText.text.toString())){
                    DBHelper(applicationContext)
                        .addBitmap(editText.text.toString(), Util.getBytes(bitmap))
                    Toast.makeText(this@SelectActivity,"Save success!", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this@SelectActivity,"Please enter name of picture!", Toast.LENGTH_SHORT).show()
                }
            }
            alertDialog.setNegativeButton("CANCEL"){
                dialog, which -> dialog.dismiss()
            }
            alertDialog.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val btn_select = findViewById<Button>(R.id.btn_select)
        val btn_save = findViewById<Button>(R.id.btn_save)
        val img_select = findViewById<ImageView>(R.id.img_select)
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK && data != null){
            val pickedImage: Uri? = data.data
            img_select.setImageURI(pickedImage)
            btn_save.isEnabled = true
        }
    }
}