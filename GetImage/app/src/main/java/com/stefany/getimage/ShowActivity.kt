package com.stefany.getimage

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.stefany.getimage.DbHelper.DBHelper
import com.stefany.getimage.Utils.Util

class ShowActivity : AppCompatActivity() {
    internal  lateinit var dbHelper: DBHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show)

        dbHelper = DBHelper((this))

        val btn_show = findViewById<Button>(R.id.btn_show)
        val edt_name = findViewById<EditText>(R.id.edt_name)
        val img_show = findViewById<ImageView>(R.id.Img_show)
        btn_show.setOnClickListener {
            if (dbHelper.getBitmapByName(edt_name.text.toString())!=null){
                val bitmap :Bitmap = Util.getImage(dbHelper.getBitmapByName(edt_name.text.toString())!!)
                img_show.setImageBitmap(bitmap)
            }else{
                Toast.makeText(this@ShowActivity, " Can not found bitmap", Toast.LENGTH_SHORT).show()
            }
        }
    }
}