package com.stefany.multipleimage

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageSwitcher
import android.widget.ImageView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    //store uris of pickes images
    private var images:ArrayList<Uri?>?=null
    private var position =0
    private val PICK_IMAGES_CODE = 100
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        images = ArrayList()
        val pickBtn = findViewById<Button>(R.id.pickBtn)
        val nextBtn = findViewById<Button>(R.id.nextBtn)
        val prevesious = findViewById<Button>(R.id.previousBtn)
        val imgSwitcher = findViewById<ImageSwitcher>(R.id.imageSwitcher)

        imgSwitcher.setFactory{ImageView(applicationContext)}

        pickBtn.setOnClickListener {
            pickImageIntent()
        }

        nextBtn.setOnClickListener {
            if(position < images!!.size-1){
                position++
                imgSwitcher.setImageURI(images!![position])
            }else{
                Toast.makeText(this,"No more image", Toast.LENGTH_SHORT).show()
            }
        }

        prevesious.setOnClickListener {
            if(position > 0){
                position--
                imgSwitcher.setImageURI(images!![position])
            }else{
                Toast.makeText(this,"No more image", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun pickImageIntent(){
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent,"Select Images"),PICK_IMAGES_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val imgSwitcher = findViewById<ImageSwitcher>(R.id.imageSwitcher)
        if(requestCode == PICK_IMAGES_CODE){
            if(requestCode == Activity.RESULT_OK){
                if(data!!.clipData != null){
                    Toast.makeText(this,"Execelen", Toast.LENGTH_LONG).show()
                    val count = data.clipData!!.itemCount
                    for (i in 0 until count){
                        val imageUri = data.clipData!!.getItemAt(i).uri
                        images!!.add(imageUri)
                    }
                    imgSwitcher.setImageURI(images!![0])
                    position = 0
                }else{
                    val imageUri = data.data
                    imgSwitcher.setImageURI(imageUri)
                    position = 0
                }
            }
        }else{
            Toast.makeText(this,"Error", Toast.LENGTH_SHORT).show()
        }
    }
}
